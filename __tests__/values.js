describe('Object.values() function', () => {
  test('should return an array of object values', () => {
    const obj = {
      name: 'John',
      age: 30,
      city: 'New York',
    };

    const values = Object.values(obj);

    expect(values).toEqual(['John', 30, 'New York']);
  });

  test('should return an empty array for an empty object', () => {
    const obj = {};

    const values = Object.values(obj);

    expect(values).toEqual([]);
  });

  test('should throw an error if the argument is not an object', () => {
    // only before es2015
    const invalidObj = '12';

    // expect(() => {
    //   Object.values(invalidObj);
    // }).toThrow();

    const values = Object.values(invalidObj);
    expect(values).toEqual(['1', '2']);
  });

  test('should work like for...in', () => {
    // except for...in takes values from prototype
    const obj = {
      name: 'John',
      age: 30,
      city: 'New York',
    };

    const values = Object.values(obj);

    const cycleValues = [];
    for (const key in obj) {
      cycleValues.push(obj[key]);
    }

    expect(values).toEqual(cycleValues);
  });

  test('should not take non-enumerable properties', () => {
    const obj = {
      name: 'John',
      age: 30,
      // city: 'New York',
    };

    Object.defineProperty(obj, 'country', {
      value: 'UK',
      // enumerable: false,
    });

    const values = Object.values(obj);
    expect(values).toEqual(['John', 30]);
  });
});
