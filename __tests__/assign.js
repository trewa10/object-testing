describe('Object.assign', () => {
  test('should merge two objects', () => {
    const obj1 = { a: 1 };
    const obj2 = { b: 2 };
    const result = Object.assign({}, obj1, obj2);
    expect(result).toEqual({ a: 1, b: 2 });
  });

  test('should overwrite properties in the first object', () => {
    const obj1 = { a: 1, b: 2 };
    const obj2 = { b: 3, c: 4 };
    const result = Object.assign(obj1, obj2);
    expect(result).toEqual({ a: 1, b: 3, c: 4 });

    // also changes the original obj1
    expect(obj1).toEqual({ a: 1, b: 3, c: 4 });
  });

  test('should return the first object', () => {
    const obj1 = { a: 1 };
    const obj2 = { b: 2 };
    const result = Object.assign(obj1, obj2);
    expect(result).toBe(obj1);
  });

  test('should not modify the second object', () => {
    const obj1 = { a: 1 };
    const obj2 = { b: 2 };
    Object.assign(obj1, obj2);
    expect(obj2).toEqual({ b: 2 });
  });

  test('should handle symbols', () => {
    const symbol1 = Symbol('symbol1');
    const obj1 = { [symbol1]: 'value1' };
    const obj2 = { [symbol1]: 'value2' };
    const result = Object.assign({}, obj1, obj2);
    expect(result).toEqual({ [symbol1]: 'value2' });
  });

  test('should not copy non-enumerable properties', () => {
    const obj1 = { a: 1 };
    const obj2 = {};
    Object.defineProperty(obj2, 'b', {
      value: 2,
      enumerable: false,
    });
    const result = Object.assign(obj1, obj2);
    expect(result).toEqual({ a: 1 });
  });

  test('should wrap primitives to objects', () => {
    const v1 = 'abc';
    const v2 = true;
    const v3 = 10;
    const v4 = Symbol('foo');
    const result = Object.assign({}, v1, null, v2, undefined, v3, v4);
    // Primitives will be wrapped, null and undefined will be ignored.
    // Only string wrappers can have own enumerable properties.
    expect(result).toEqual({ 0: 'a', 1: 'b', 2: 'c' });
  });
});
