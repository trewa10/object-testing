describe('Object.getOwnPropertySymbols()', () => {
  test('should return an array of own symbols of the object', () => {
    const nameSymbol = Symbol('name');
    const ageSymbol = Symbol('age');
    const obj = {
      [nameSymbol]: 'John',
      [ageSymbol]: 30,
    };
    const symbols = Object.getOwnPropertySymbols(obj);
    expect(symbols).toEqual([nameSymbol, ageSymbol]);
  });

  test('should return an empty array for the object with no symbols', () => {
    const obj = {
      name: 'John',
      age: 30,
    };
    const symbols = Object.getOwnPropertySymbols(obj);
    expect(symbols).toEqual([]);
  });

  test('should throw TypeError for null or undefined', () => {
    expect(() => {
      Object.getOwnPropertySymbols(null);
    }).toThrow(TypeError);

    expect(() => {
      Object.getOwnPropertySymbols(undefined);
    }).toThrow(TypeError);
  });

  test('should return symbols including non-enumerable symbols', () => {
    const nameSymbol = Symbol('name');
    const ageSymbol = Symbol('age');

    const obj = {
      [ageSymbol]: 30,
    };
    Object.defineProperty(obj, nameSymbol, {
      value: 'John',
      enumerable: false,
    });
    const symbols = Object.getOwnPropertySymbols(obj);
    expect(symbols).toEqual([ageSymbol, nameSymbol]);
  });

  test('should not return names for inherited properties', () => {
    const parentSymbol = Symbol('parent');
    const childSymbol = Symbol('child');

    const parent = { [parentSymbol]: 'parentValue' };
    const child = Object.create(parent, {
      [childSymbol]: {
        value: 'childValue',
        writable: false,
        enumerable: true,
        configurable: false,
      },
    });
    const symbols = Object.getOwnPropertySymbols(child);
    expect(symbols).toEqual([childSymbol]);
  });
});

