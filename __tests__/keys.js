describe('Object.keys() function', () => {
  test('should return an array of object keys', () => {
    const obj = {
      name: 'John',
      age: 30,
      city: 'New York',
    };

    const keys = Object.keys(obj);

    expect(keys).toEqual(['name', 'age', 'city']);
  });

  test('should return an empty array for an empty object', () => {
    const obj = {};

    const keys = Object.keys(obj);

    expect(keys).toEqual([]);
  });

  test('should throw an error if the argument is not an object', () => {
    // only before es2015
    const invalidObj = '12';

    // expect(() => {
    //   Object.keys(invalidObj);
    // }).toThrow();

    const keys = Object.keys(invalidObj);
    expect(keys).toEqual(['0', '1']);
  });

  test('should work like for...in', () => {
    // except for...in takes values from prototype
    const obj = {
      name: 'John',
      age: 30,
      city: 'New York',
    };

    const keys = Object.keys(obj);

    const cycleKeys = [];
    for (const key in obj) {
      cycleKeys.push(key);
    }

    expect(keys).toEqual(cycleKeys);
  });

  test('should not take non-enumerable properties', () => {
    const obj = {
      name: 'John',
      age: 30,
      city: 'New York',
    };

    Object.defineProperty(obj, 'city', {
      value: 'London',
      enumerable: false,
    });

    const keys = Object.keys(obj);
    expect(keys).toEqual(['name', 'age']);
  });
});
