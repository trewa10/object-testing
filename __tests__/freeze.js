'use strict'; // for throwing errors

describe('Object.freeze() and Object.isFrozen() functions', () => {
  test('should freeze an object and prevent any changes', () => {
    const obj = { foo: 'bar' };
    Object.freeze(obj);

    expect(() => {
      obj.foo = 'baz';
    }).toThrow();

    expect(obj).toEqual({ foo: 'bar' });
    expect(Object.isFrozen(obj)).toBe(true);
  });

  test('should return true for a frozen object and false otherwise', () => {
    const obj1 = { foo: 'bar' };
    const obj2 = Object.freeze({ foo: 'bar' });
    const obj3 = Object.freeze({});

    expect(Object.isFrozen(obj1)).toBe(false);
    expect(Object.isFrozen(obj2)).toBe(true);
    expect(Object.isFrozen(obj3)).toBe(true);
  });

  test('should return true for a frozen object and false otherwise, excluding nested objects', () => {
    const obj1 = {
      foo: 'bar',
      nested: {
        baz: 42,
      },
    };
    Object.freeze(obj1);

    expect(Object.isFrozen(obj1)).toBe(true);
    expect(Object.isFrozen(obj1.nested)).toBe(false);
  });

  test('should freeze an object and prevent any changes, excluding nested objects', () => {
    const obj = {
      foo: 'bar',
      nested: {
        baz: 42,
      },
    };
    Object.freeze(obj);

    expect(() => {
      obj.foo = 'baz';
    }).toThrow();

    expect(() => {
      obj.nested.baz = 0;
    }).not.toThrow();

    expect(obj).toEqual({
      foo: 'bar',
      nested: {
        baz: 0,
      },
    });

    Object.freeze(obj.nested);

    expect(Object.isFrozen(obj)).toBe(true);
    expect(Object.isFrozen(obj.nested)).toBe(true);
  });
});
