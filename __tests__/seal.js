'use strict'; // for throwing errors

describe('Object.seal() and isSealed()', () => {
  test('should prevent adding new properties to the object', () => {
    const obj = { name: 'John' };
    Object.seal(obj);
    expect(Object.isSealed(obj)).toBe(true);
    // obj.age = 30;
    expect(() => obj.age = 30).toThrow();
    expect(obj).toEqual({ name: 'John' });
  });

  test('should prevent deleting properties from the object', () => {
    const obj = { name: 'John' };
    Object.seal(obj);
    // delete obj.name;
    expect(() => delete obj.name).toThrow();
    expect(obj).toEqual({ name: 'John' });
  });

  test('should allow modifying existing properties of the object', () => {
    const obj = { name: 'John' };
    Object.seal(obj);
    obj.name = 'Bob';
    expect(obj).toEqual({ name: 'Bob' });
  });

  test('should not convert data properties to accessors', () => {
    const obj = { name: 'John' };
    Object.seal(obj);
    expect(() => Object.defineProperty(obj, "name", {
      get() {
        return "Mike";
      },
    })).toThrow();
    expect(Object.getOwnPropertyDescriptor(obj, 'name').configurable).toBe(false);
  });

  test('should make all properties non-configurable', () => {
    const obj = { name: 'John' };
    Object.seal(obj);
    expect(Object.getOwnPropertyDescriptor(obj, 'name').configurable).toBe(false);
  });

  test('should seal an object, excluding nested objects', () => {
    const obj = {
      name: 'John',
      nested: {
        age: 42,
      },
    };
    Object.seal(obj);

    expect(() => {
      obj.foo = 'bar';
    }).toThrow();

    expect(() => {
      obj.nested.baz = 0;
    }).not.toThrow();

    expect(obj).toEqual({
      name: 'John',
      nested: {
        age: 42,
        baz: 0
      },
    });

    Object.seal(obj.nested);

    expect(Object.isSealed(obj)).toBe(true);
    expect(Object.isSealed(obj.nested)).toBe(true);
  });
});
