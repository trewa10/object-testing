describe('Object.getPrototypeOf()', () => {
  test('should return the prototype of an object', () => {
    const parent = { name: 'John' };
    const child = Object.create(parent);
    const prototype = Object.getPrototypeOf(child);
    expect(prototype).toEqual(parent);
  });

  test('should return Object.prototype for objects created with literal notation', () => {
    const obj = { name: 'John' };
    const prototype = Object.getPrototypeOf(obj);
    expect(prototype).toEqual(Object.prototype);
  });

  test('should return null for objects created with Object.create(null)', () => {
    const obj = Object.create(null);
    const prototype = Object.getPrototypeOf(obj);
    expect(prototype).toEqual(null);
  });

  test('should throw TypeError for null or undefined', () => {
    expect(() => {
      Object.getPrototypeOf(null);
    }).toThrow(TypeError);

    expect(() => {
      Object.getPrototypeOf(undefined);
    }).toThrow(TypeError);
  });

  test('should return prototype for non objects', () => {
    const prototype = Object.getPrototypeOf('obj');
    expect(prototype).toEqual(String.prototype);

    const prototype2 = Object.getPrototypeOf(['obj']);
    expect(prototype2).toEqual(Array.prototype);

    const prototype3 = Object.getPrototypeOf(123);
    expect(prototype3).toEqual(Number.prototype);

    const prototype4 = Object.getPrototypeOf(true);
    expect(prototype4).toEqual(Boolean.prototype);
  });
});
