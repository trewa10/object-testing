
describe('Object.preventExtensions()', () => {
  let obj;

  beforeEach(() => {
    obj = { name: 'John' };
  });

  test('should prevent adding new properties', () => {
    Object.preventExtensions(obj);
    obj.age = 30;
    expect(obj.age).toBeUndefined();
  });

  test('should throw TypeError when attempting to add new properties in strict mode', () => {
    'use strict';
    Object.preventExtensions(obj);
    expect(() => {
      obj.age = 30;
    }).toThrow(TypeError);
  });

  test('should return the same object', () => {
    const result = Object.preventExtensions(obj);
    expect(result).toBe(obj);
  });

  test('should not affect existing properties', () => {
    Object.preventExtensions(obj);
    obj.name = 'Mike';
    expect(obj.name).toBe('Mike');
  });

  test('should allow deleting existing properties', () => {
    Object.preventExtensions(obj);
    delete obj.name;
    expect(obj.name).toBeUndefined();
  });

  test('should work with strings', () => {
    let str = Object.preventExtensions('Hello');
    expect(Object.isExtensible(str)).toBe(false);
  });


  describe('Object.isExtensible()', () => {
    test('should return true for new objects', () => {
      expect(Object.isExtensible({})).toBe(true);
    });

    test('should return false for objects created with Object.preventExtensions()', () => {
      Object.preventExtensions(obj);
      expect(Object.isExtensible(obj)).toBe(false);
    });

    test('should return false for objects created with Object.seal()', () => {
      const sealedObj = Object.seal({});
      expect(Object.isExtensible(sealedObj)).toBe(false);
    });

    test('should return false for objects created with Object.freeze()', () => {
      const frozenObj = Object.freeze({});
      expect(Object.isExtensible(frozenObj)).toBe(false);
    });
  });
});
