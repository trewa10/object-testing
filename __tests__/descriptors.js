describe('Object.getOwnPropertyDescriptors()', () => {
  test('should return descriptors for all own properties of an object excluding nested', () => {
    const obj = {
      name: 'John',
      age: 35,
      adress: { nestedProp: 'nestedValue' },
    };
    const descriptors = Object.getOwnPropertyDescriptors(obj);
    expect(descriptors).toEqual({
      name: {
        value: 'John',
        writable: true,
        enumerable: true,
        configurable: true,
      },
      age: {
        value: 35,
        writable: true,
        enumerable: true,
        configurable: true,
      },
      adress: {
        value: { nestedProp: 'nestedValue' },
        writable: true,
        enumerable: true,
        configurable: true,
      },
    });
  });

  test('should return undefined for non-existent properties', () => {
    const obj = { prop: 'value' };
    const descriptors = Object.getOwnPropertyDescriptors(obj);
    expect(descriptors.nonExistentProp).toBeUndefined();
  });

  test('should return an empty descriptors object for an empty object', () => {
    const obj = {};
    const descriptors = Object.getOwnPropertyDescriptors(obj);
    expect(descriptors).toEqual({});
  });

  test('should return descriptors for non-enumerable properties', () => {
    const obj = {};
    Object.defineProperty(obj, 'nonEnumerable', {
      value: 'value',
      // enumerable: false,
    });
    const descriptors = Object.getOwnPropertyDescriptors(obj);
    expect(descriptors.nonEnumerable).toEqual({
      value: 'value',
      writable: false,
      enumerable: false,
      configurable: false,
    });
  });

  test('should not return descriptors for inherited properties', () => {
    const parent = { parentProp: 'parentValue' };
    const child = Object.create(parent, {
      childProp: {
        value: 'childValue',
        writable: false,
        enumerable: true,
        configurable: false,
      },
    });
    const descriptors = Object.getOwnPropertyDescriptors(child);
    expect(descriptors.childProp).toEqual({
      value: 'childValue',
      writable: false,
      enumerable: true,
      configurable: false,
    });
    expect(descriptors.parentProp).toBeUndefined();
  });
});
