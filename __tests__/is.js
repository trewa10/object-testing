/* 
  The only difference between Object.is() and === is in their treatment of signed zeros and NaN values. 
  The === operator (and the == operator) treats the number values -0 and +0 as equal,
    but treats NaN as not equal to each other.
*/

describe('Object.is()', () => {

  describe('should return true', () => {
    test('when comparing two NaN values', () => {
      expect(Object.is(NaN, NaN)).toBe(true);
    });

    test('when comparing two positive zeros', () => {
      expect(Object.is(+0, +0)).toBe(true);
    });

    test('when comparing two negative zeros', () => {
      expect(Object.is(-0, -0)).toBe(true);
    });

    test('when comparing two null and undefined', () => {
      expect(Object.is(null, null)).toBe(true);
      expect(Object.is(undefined, undefined)).toBe(true);
    });

    test('when comparing two equal non-zero numbers', () => {
      expect(Object.is(42, 42)).toBe(true);
    });

    test('when comparing two equal strings', () => {
      expect(Object.is('hello', 'hello')).toBe(true);
    });

    test('when comparing two equal objects', () => {
      const obj1 = { a: 1 };
      const obj2 = obj1;
      expect(Object.is(obj1, obj2)).toBe(true);
    });

    test('when comparing two equal arrays', () => {
      const arr1 = [1, 2, 3];
      const arr2 = arr1;
      expect(Object.is(arr1, arr2)).toBe(true);
    });

    test('when comparing two equal functions', () => {
      const func1 = function() { return 42; };
      const func2 = func1;
      expect(Object.is(func1, func2)).toBe(true);
    });

    test('when comparing a value with itself', () => {
      const x = 42;
      expect(Object.is(x, x)).toBe(true);
    });
  });

  describe('should return false', () => {
    test('when comparing a positive zero with a negative zero', () => {
      expect(Object.is(+0, -0)).toBe(false);
    });

    test('when comparing a non-zero number with NaN', () => {
      expect(Object.is(42, NaN)).toBe(false);
    });

    test('when comparing two different strings', () => {
      expect(Object.is('hello', 'Hello')).toBe(false);
    });

    test('when comparing two different objects', () => {
      const obj1 = { a: 1 };
      const obj2 = { a: 1 };
      expect(Object.is(obj1, obj2)).toBe(false);
    });

    test('when comparing two different arrays', () => {
      const arr1 = [1, 2, 3];
      const arr2 = [1, 2, 3];
      expect(Object.is(arr1, arr2)).toBe(false);
    });

    test('when comparing two different functions', () => {
      const func1 = function() { return 42; };
      const func2 = function() { return 42; };
      expect(Object.is(func1, func2)).toBe(false);
    });

    test('when comparing a value with a different type', () => {
      expect(Object.is(42, '42')).toBe(false);
    });

    test('when comparing a value with null or undefined', () => {
      expect(Object.is(42, null)).toBe(false);
      expect(Object.is(42, undefined)).toBe(false);
      expect(Object.is(null, undefined)).toBe(false);
    });
  });

  describe('should ignore third and next arguments', () => {
    test('when comparing values', () => {
      expect(Object.is(NaN, NaN, 1, '42')).toBe(true);
    });
  })

});
