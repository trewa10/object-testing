describe('Object.getOwnPropertyNames()', () => {
  test('should return an array of own property names of an object exclude nested', () => {
    const obj = {
      name: 'John',
      age: 30,
      address: {
        street: '123 Main St',
        city: 'New York',
      },
    };
    const names = Object.getOwnPropertyNames(obj);
    expect(names).toEqual(['name', 'age', 'address']);
  });

  test('should return an empty array for an empty object', () => {
    const obj = {};
    const names = Object.getOwnPropertyNames(obj);
    expect(names).toEqual([]);
  });

  test('should throw Error for null or undefined', () => {
    expect(() => {
      Object.getOwnPropertyNames(null);
    }).toThrow(TypeError);

    expect(() => {
      Object.getOwnPropertyNames(undefined);
    }).toThrow(TypeError);
  });

  test('should return property names including non-enumerable properties', () => {
    const obj = {};
    Object.defineProperty(obj, 'name', {
      value: 'John',
      enumerable: false,
    });
    const names = Object.getOwnPropertyNames(obj);
    expect(names).toEqual(['name']);
  });

  test('should return property names for array', () => {
    const arr = ['a', 'b', 'c'];
    const names = Object.getOwnPropertyNames(arr);
    expect(names).toEqual(['0', '1', '2', 'length']);
  });

  test('should not return names for inherited properties', () => {
    const parent = { parentProp: 'parentValue' };
    const child = Object.create(parent, {
      childProp: {
        value: 'childValue',
        writable: false,
        enumerable: true,
        configurable: false,
      },
    });
    const names = Object.getOwnPropertyNames(child);
    expect(names).toEqual(['childProp']);
  });

  test('should be used for getting only nonenumerable keys', () => {
    const obj = {
      age: 30,
      address: 'address',
    };
    Object.defineProperty(obj, 'name', {
      value: 'John',
      enumerable: false,
    });
    Object.defineProperty(obj, 'country', {
      value: 'USA',
      enumerable: false,
    });
    const allNames = Object.getOwnPropertyNames(obj);
    const enumNames = Object.keys(obj);
    const nonenumNames = allNames.filter(name => !enumNames.includes(name));
    expect(nonenumNames).toEqual(['name', 'country']);
  });
});
