describe('Object.entries() function', () => {
  test('should return an array of key-value pairs', () => {
    const obj = {
      name: 'John',
      age: 30,
      city: 'New York'
    };

    const entries = Object.entries(obj);

    expect(entries).toEqual([
      ['name', 'John'],
      ['age', 30],
      ['city', 'New York']
    ]);
  });

  test('should return an empty array for an empty object', () => {
    const obj = {};

    const entries = Object.entries(obj);

    expect(entries).toEqual([]);
  });

  test('should throw an error if the argument is not an object', () => {
    // only before es2015
    const invalidObj = 'abc';

    // expect(() => {
    //   Object.entries(invalidObj);
    // }).toThrow();

    const entries = Object.entries(invalidObj);
    expect(entries).toEqual( [ [ '0', 'a' ], [ '1', 'b' ], [ '2', 'c' ] ]);
  });

  test('should work like for...in', () => {
    // except for...in takes entries from prototype
    const obj = {
      name: 'John',
      age: 30,
      city: 'New York',
    };

    const entries = Object.entries(obj);

    const cycleEntries = [];
    for (const key in obj) {
      cycleEntries.push([key, obj[key]]);
    }

    expect(entries).toEqual(cycleEntries);
  });

  test('should not take non-enumerable properties', () => {
    const obj = {
      name: 'John',
      age: 30,
      city: 'New York',
    };

    Object.defineProperty(obj, 'city', {
      value: 'London',
      enumerable: false,
    });

    const entries = Object.entries(obj);
    expect(entries).toEqual([ ['name', 'John'], ['age', 30] ]);
  });

  test('should create a new Map from an object', () => {
    const obj = { name: 'John', age: 30 };

    const map = new Map(Object.entries(obj));

    expect(map).toEqual(new Map([
      ['name', 'John'],
      ['age', 30]
    ]));
  });

  test('should log each key-value pair in the object', () => {
    const obj = { name: 'John', age: 42 };

    // spyOn() method creates a mock of the console.log() method
    const consoleSpy = jest.spyOn(console, 'log');

    Object.entries(obj).forEach(([key, value]) => console.log(`${key}: ${value}`));

    expect(consoleSpy).toHaveBeenCalledWith('name: John');
    expect(consoleSpy).toHaveBeenCalledWith('age: 42');

    // restore the original console.log() method
    consoleSpy.mockRestore();
  });
});
