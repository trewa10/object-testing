describe('Object.getOwnPropertyDescriptor() method', () => {
  test('should return the descriptor of an existing property in the object', () => {
    const obj = { foo: 'bar' };
    const descriptor = Object.getOwnPropertyDescriptor(obj, 'foo');
    expect(descriptor).toEqual({
      value: 'bar',
      writable: true,
      enumerable: true,
      configurable: true
    });
  });

  test('should return undefined when property does not exist in the object', () => {
    const obj = { foo: 'bar' };
    const descriptor = Object.getOwnPropertyDescriptor(obj, 'baz');
    expect(descriptor).toBeUndefined();
  });

  test('should not return the descriptor of a property in prototype', () => {
    function MyObject() {}
    MyObject.prototype.foo = 'bar';
    const obj = new MyObject();
    const descriptor = Object.getOwnPropertyDescriptor(obj, 'foo');
    expect(descriptor).toBeUndefined();
  });

  test('should return the descriptor of a non-enumerable property', () => {
    const obj = {};
    Object.defineProperty(obj, 'foo', {
      value: 'bar',
      // writable: true,
      // enumerable: false,
      // configurable: true
    });
    const descriptor = Object.getOwnPropertyDescriptor(obj, 'foo');
    expect(descriptor).toEqual({
      value: 'bar',
      writable: false,
      enumerable: false,
      configurable: false
    });
  });

  // Test case 5: Accessor property
  test('should return the descriptor of a getter property', () => {
    const obj = {
      get foo() { return 'bar'; }
    };
    const descriptor = Object.getOwnPropertyDescriptor(obj, 'foo');
    expect(descriptor.get).toBeInstanceOf(Function);
    expect(descriptor.set).toBeUndefined();
    expect(descriptor.enumerable).toEqual(true);
    expect(descriptor.configurable).toEqual(true);
  });

  test('should return the descriptor of a setter property', () => {
    const obj = {
      set foo(val) { this._foo = val; },
      _foo: ''
    };
    const descriptor = Object.getOwnPropertyDescriptor(obj, 'foo');
    expect(descriptor.set).toBeInstanceOf(Function);
    expect(descriptor.get).toBeUndefined();
    expect(descriptor.enumerable).toEqual(true);
    expect(descriptor.configurable).toEqual(true);
  });

  test('should return the descriptor of an accessor and setter property', () => {
    const obj = {
      get foo() { return this._foo; },
      set foo(val) { this._foo = val; },
      _foo: ''
    };
    const descriptor = Object.getOwnPropertyDescriptor(obj, 'foo');
    expect(descriptor.get).toBeInstanceOf(Function);
    expect(descriptor.set).toBeInstanceOf(Function);
    expect(descriptor.enumerable).toEqual(true);
    expect(descriptor.configurable).toEqual(true);
  });

  test('should return the descriptor of a property defined with Object.create()', () => {
    const parent = { foo: 'bar' };
    const obj = Object.create(parent, {
      baz: {
        value: 'qux',
        writable: true,
        enumerable: true,
        configurable: true
      }
    });
    const descriptor = Object.getOwnPropertyDescriptor(obj, 'baz');
    expect(descriptor).toEqual({
      value: 'qux',
      writable: true,
      enumerable: true,
      configurable: true
    });
  });
});
